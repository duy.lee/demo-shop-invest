<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::group(['prefix' => 'admin',  'middleware' => 'auth'], function () {
    Route::resource('products', 'ProductController');
});

Route::get('product-detail/{id}', 'HomeController@productDetail')->name('product-detail');

Route::post('add-to-cart', 'HomeController@addToCart')->name('add-to-cart');