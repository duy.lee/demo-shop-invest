<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'price',
        'brand_id',
        'description',
        'image_id',
    ];

    /**
     * Get the brand that owns the product.
     */
    public function brand()
    {
        return $this->belongsTo(brand::class);
    }

    /**
     * Get the image that owns the product.
     */
    public function image()
    {
        return $this->hasOne(Image::class);
    }
}
