<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Image;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Cache::remember('products', 86400, function () {
            return Product::with('brand', 'image')->paginate(20);
        });

        $brands = Cache::remember('brands', 86400, function () {
            return Brand::get();
        });
    
        return view('admin.product.index', compact('products', 'brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'bail|required|string',
            'price' => 'bail|required|numeric',
            'image' => 'bail|required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'brand_id' => 'bail|required|exists:brands,id',
            'description' => 'bail|required|string',
        ]);

        if ($validator->passes()) {

            $product = Product::create([
                'name' => $request->name,
                'price' => $request->price,
                'brand_id' => $request->brand_id,
                'description' => $request->description,
            ]);

            // save image
            if($product && isset($request->image)) {
                $file_name = $request->image->getClientOriginalName();
                $name = pathinfo($file_name, PATHINFO_FILENAME);
                $extension = pathinfo($file_name, PATHINFO_EXTENSION);

                $imageName = $name.'_'.time() .'.'. $extension;  
                $request->image->move(public_path('images'), $imageName);

                // save image model
                $image = new Image(['product_id' => $product->id, 'filename' => $imageName]);
                $product->image()->save($image);
            }

            return back()->with('success', __('Created successfully'));
        } else {
            return back()->withErrors($validator);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $brands = Brand::get();
        return view('admin.product.edit', compact('product', 'brands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'bail|required|string',
            'price' => 'bail|required|numeric',
            'image' => 'bail|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'brand_id' => 'bail|required|exists:brands,id',
            'description' => 'bail|required|string',
        ]);

        if ($validator->passes()) {

            if(isset($request->image)) {

                // delete old image
                if(file_exists(public_path('images/' . $product->image->filename))) {
                    unlink(public_path('images/' . $product->image->filename));
                }

                // save image
                $file_name = $request->image->getClientOriginalName();
                $name = pathinfo($file_name, PATHINFO_FILENAME);
                $extension = pathinfo($file_name, PATHINFO_EXTENSION);

                $imageName = $name.'_'.time() .'.'. $extension;  
                $request->image->move(public_path('images'), $imageName);

                $product->image()->update([
                    'filename' => $imageName
                ]);
            }
            
            $product->update($request->only('name', 'price', 'brand_id', 'description'));

            return back()->with('success', __('Updated successfully'));
        } else {
            return back()->withErrors($validator);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return back()->with('success', __('Deleted successfully'));
    }
}
