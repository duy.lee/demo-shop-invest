<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\View;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $products = Cache::remember('products', 86400, function () {
            return Product::with('brand', 'image')->paginate(20);
        });

        return view('welcome', compact('products'));
    }

    public function productDetail($id)
    {
        $product = Product::with('image', 'brand')->findOrFail($id);
        return view('product-detail', compact('product'));
    }

    public function addToCart(Request $request)
    {
        $id = $request->product_id;
        $qty = $request->quantity;
        $product = Product::findOrFail($id);
        
        $carts = session()->get('cart');

        if(isset($carts[$id])) {
            $carts[$id]['quantity'] = $qty;
            session()->put('cart', $carts);

        } else {
            $carts[$id] = [
                'name' => $product->name,
                'quantity' => $qty,
                'price' => $product->price,
                'image' => $product->image->filename
            ];

            $carts = session()->put('cart', $carts);
        }

        $cart_html = View::make('includes.cart', ['carts' => $carts])->render();

        return response()->json([
            'message'   => __('Add to cart successfully'),
            'cart_fragment' => $cart_html
        ], Response::HTTP_OK);
    }
}
