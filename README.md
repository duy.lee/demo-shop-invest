## Installation ShopInvest

1. copy .env.example
```
cp .env.example .env
```

2. Create database

3. install composer
```
composer install
```

3. migrate and seeder
```
php artisan migrate --seed
```

5. start server
```
php artisan serve
```

## path and account admin

- admin product: /admin/products
- fontend product: /

- Account:
    + email: admin@admin.com
    + pass:  password

