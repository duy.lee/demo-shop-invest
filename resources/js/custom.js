
(function ($) {
    "use strict";

    $(document).on("keypress", "#inputPrice", function(ev) {
        if (!/^[0-9]*\.?[0-9]*$/.test(ev.key)) ev.preventDefault();
    });

    $(document).on('click', '.btn-plus, .btn-minus', function(e) {
        const isNegative = $(e.target).closest('.btn-minus').is('.btn-minus');
        const input = $(e.target).closest('.input-group').find('input');
        if (input.is('input')) {
          input[0][isNegative ? 'stepDown' : 'stepUp']()
        }
    });

})(jQuery);
