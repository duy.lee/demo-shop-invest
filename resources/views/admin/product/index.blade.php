@extends('layouts.app')

@section('content')
<div class="container">

    @include('admin.includes.alert')

    <a type="button" class="btn btn-success" data-toggle="modal" data-target=".bd-modal-add-new">Add new</a>

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Price</th>
                        <th scope="col">Brand</th>
                        <th scope="col">Image</th>
                        <th></th>
                      </tr>
                    </thead>

                    <tbody>
                        @foreach ($products as $key => $product)
                            <tr>
                                <th scope="row">{{ $key + 1 }}</th>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->price }}</td>
                                <td>{{ isset($product->brand->name) ? $product->brand->name : '' }}</td>
                                <td>
                                    @if (isset($product->image->filename))
                                        <img class="img-thumbnail" src="{{ asset('images/' . $product->image->filename) }}" style="max-width: 100px;">
                                    @endif
                                </td>
                                <td class="text-right">
                                    <a type="button" href="{{ route('products.edit', $product) }}" class="btn btn-primary btnEditProduct">edit</a>
                                    
                                    <form class="d-inline" action="{{route('products.destroy', $product)}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        
                                        <button type="submit" class="btn btn-danger">delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach                    
                    </tbody>
                </table>

                @if(!count($products))
                    <p class="px-3">No data!</p>
                @endif

            </div>

            {!! $products->links() !!}
        </div>
    </div>
</div>


{{-- modal add new product --}}
<div class="modal fade bd-modal-add-new" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Add new product</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            
            <form action="{{ route('products.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                
                <div class="modal-body">
                    <div class="mb-3">
                        <label for="inputName" class="form-label">Name</label>
                        <input type="text" name="name" class="form-control" id="inputName">
                    </div>
                    <div class="mb-3">
                        <label for="inputPrice" class="form-label">Price</label>
                        <input type="text" name="price" class="form-control" id="inputPrice">
                    </div>
                    <div class="mb-3">
                        <label for="inputImage" class="form-label">Image</label>
                        <input type="file" name="image" class="form-control-file" id="inputImage">
                    </div>
                    <div class="mb-3">
                        <label for="inputBrand" class="form-label">Brand</label>
                        <select name="brand_id" class="form-control" id="inputBrand">
                            <option>Selected</option>
                            @foreach ($brands as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="inputDesc" class="form-label">Description</label>
                        <textarea name="description" class="form-control" id="inputDesc" rows="3"></textarea>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection