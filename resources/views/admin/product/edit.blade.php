@extends('layouts.app')

@section('content')
<div class="container">

    @include('admin.includes.alert')

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                
                <form action="{{ route('products.update', $product) }}" method="POST" enctype="multipart/form-data">
                    @method('PATCH')
                    @csrf
                    
                    <div class="modal-body">
                        <div class="mb-3">
                            <label for="inputName" class="form-label">Name</label>
                            <input type="text" name="name" class="form-control" id="inputName" value="{{ $product->name }}">
                        </div>
                        <div class="mb-3">
                            <label for="inputPrice" class="form-label">Price</label>
                            <input type="text" name="price" class="form-control" id="inputPrice" value="{{ $product->price }}">
                        </div>
                        <div class="mb-3">
                            <label for="inputImage" class="form-label">Image</label>
                            <input type="file" name="image" class="form-control-file" id="inputImage">
                            @if (isset($product->image->filename))
                                <img class="img-thumbnail" src="{{ asset('images/' . $product->image->filename) }}" style="max-width: 100px;">
                            @endif
                        </div>
                        <div class="mb-3">
                            <label for="inputBrand" class="form-label">Brand</label>
                            <select name="brand_id" class="form-control" id="inputBrand">
                                <option>Selected</option>
                                @foreach ($brands as $item)
                                    <option value="{{ $item->id }}" {{ ($product->brand->id == $item->id) ? 'selected' : '' }}>{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="inputDesc" class="form-label">Description</label>
                            <textarea name="description" class="form-control" id="inputDesc" rows="3">{{ $product->description }}</textarea>
                        </div>
                        
                    </div>
                    <div class="modal-footer">
                        <a type="button" href="{{ route('products.index') }}" class="btn btn-secondary" data-dismiss="modal">Back</a>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
    <script type="text/javascript">
        

    </script>
@endpush