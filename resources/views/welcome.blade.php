@extends('layouts.app-fontend')

@section('content')
<div class="container">

    @include('admin.includes.alert')

    <div class="row">
        
        @foreach ($products as $product)
            <div class="col-md-4">
                <div class="card card-product">
                    <img src="{{ asset('images/'.$product->image->filename) }}" class="card-img-top card-product__image" alt="{{ $product->title }}">
                    <div class="card-body">
                        <h5 class="card-title">{{ $product->title }}</h5>
                        <p class="card-text card-product__desc">{{ $product->description }}</p>
                        <p>
                            <span class="mr-1">
                                <strong>€ {{ $product->price }}</strong>
                            </span>
                        </p>
                        <a href="{{ route('product-detail', $product) }}" class="btn btn-primary">{{ __('Product detail') }}</a>
                    </div>
                </div>
            </div>
        @endforeach
    
    </div>
    {!! $products->links() !!}
    
</div>

@endsection