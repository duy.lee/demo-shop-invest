
@php
if(!isset($carts)) {
    $carts = session()->get('cart');
}
@endphp

<div class="col-md-12" id="cartContent">
    @empty(!$carts)
        @foreach ($carts as $id => $cart)
            <div class="d-sm-flex justify-content-between my-4 pb-4 border-bottom">
                <div class="media d-block d-sm-flex text-center text-sm-left">
                    <a class="cart-item-thumb mx-auto mr-sm-4" href="{{ route('product-detail', $id) }}">
                        <img src="{{ asset('images/'.$cart['image']) }}" alt="" style="max-width: 100px;">
                    </a>
                    <div class="media-body">
                        <h3 class="product-card-title font-weight-semibold border-0 pb-0"><a href="#">{{ $cart['name'] }}</a>
                        </h3>
                        <div class="font-size-sm"><span class="text-muted mr-2">Quantity:</span>{{ $cart['quantity'] }}</div>
                        <div class="font-size-lg text-primary pt-2">€ {{ $cart['price'] }}</div>
                    </div>
                </div>
                <div class="pt-2 pt-sm-0 pl-sm-3 mx-auto mx-sm-0 text-center text-sm-left" style="max-width: 10rem;">
                    <div class="form-group mb-2">
                        <button class="btn btn-outline-danger btn-sm btn-block mb-2 removeProductCart" type="button">Remove</button>
                    </div>
                </div>
            </div>
        @endforeach
    
    @else
        <p>No cart!</p>
    @endempty
    
</div>