@extends('layouts.app-fontend')

@section('content')
<div class="container">

    @include('admin.includes.alert')

    <div class="row justify-content-center">
        <div class="col-md-6 text-center">
            <img src="{{ asset('images/'.$product->image->filename) }}" class="img-fluid" alt="...">
        </div>
        <div class="col-md-6">
            <h2>{{ $product->name }}</h2>
            <p class="mb-2 text-muted text-uppercase small">{{ $product->brand->name }}</p>
            <p>
                <span class="mr-1">
                    <strong>€ {{ $product->price }}</strong>
                </span>
            </p>

            <hr>
            <div class="table-responsive mb-2" id="productInfo" data-id="{{ $product->id }}">
                <table class="table table-sm table-borderless">
                    <tbody>
                        <tr>
                            <td class="pl-0 pb-0 w-25">Quantity</td>
                        </tr>
                        <tr>
                            <td class="pl-0">
                                <div class="input-group inline-group">
                                    <div class="input-group-prepend">
                                      <button class="btn btn-outline-secondary btn-minus">
                                        <i class="fa fa-minus"></i>
                                      </button>
                                    </div>
                                    <input class="form-control product-quantity" min="1" name="quantity" value="1" type="number">
                                    <div class="input-group-append">
                                      <button class="btn btn-outline-secondary btn-plus">
                                        <i class="fa fa-plus"></i>
                                      </button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            
            <button type="button" id="addProductToCart" class="btn btn-primary btn-md mr-1 mb-2">Add to cart</button>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 mt-5">
            <ul class="nav nav-tabs mb-3" id="pills-tab" role="tablist">
                <li class="nav-item" role="presentation">
                  <a class="nav-link active" id="pills-description-tab" data-toggle="pill" href="#pills-description" role="tab" aria-controls="pills-description" aria-selected="true">Description</a>
                </li>
                <li class="nav-item" role="presentation">
                  <a class="nav-link" id="pills-delivery-tab" data-toggle="pill" href="#pills-delivery" role="tab" aria-controls="pills-delivery" aria-selected="false">Delivery</a>
                </li>
                <li class="nav-item" role="presentation">
                  <a class="nav-link" id="pills-information-tab" data-toggle="pill" href="#pills-information" role="tab" aria-controls="pills-information" aria-selected="false">Information</a>
                </li>
              </ul>
              <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-description" role="tabpanel" aria-labelledby="pills-description-tab">
                    {!! $product->description !!}
                </div>
                <div class="tab-pane fade" id="pills-delivery" role="tabpanel" aria-labelledby="pills-delivery-tab">...</div>
                <div class="tab-pane fade" id="pills-information" role="tabpanel" aria-labelledby="pills-information-tab">...</div>
              </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script type="text/javascript">

    $(document).ready(function() {

        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });

        $(document).on('click', '#addProductToCart', function(e) {
            var id = $('#productInfo').data('id');
            var qty = parseInt($('#productInfo .product-quantity').val());

            $.ajax({
                type: "POST",
                url: '{{ route("add-to-cart") }}',
                data: {
                    product_id: id,
                    quantity: qty
                },
                dataType: "json",
                success: function (data) {
                    $('#cartContent').replaceWith(data.cart_fragment);

                    $('#cartModal').modal('show');
                },
                
                error: function (error) {
                    alert(error.message);
                }
            });
        });
    })
    
</script>
@endpush