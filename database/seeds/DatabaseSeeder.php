<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // create user admin
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('password'),
            'is_admin'  => true
        ]);

        // create user admin
        DB::table('brands')->insert([
            ['name' => 'brand 1'], ['name' => 'brand 2'], ['name' => 'brand 3']
        ]);
    }
}
